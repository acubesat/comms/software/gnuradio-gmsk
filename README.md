# GMSK Modulator and Demodulator

## Background

### MSK

Minimum-Shift Keying (MSK) is a type of Continuous Phase Frequency Shift Keying (CPFK) modulation where the signal can be expressed as:

```math
s(t) = \sqrt{\frac{2E}{T}}\cos(2\pi f_c t + \phi(t) + \theta), \ nT \leq t < (n+1)T
```

where $`b_n`$ are the NRZ-encoded symbols of the input binary stream, $`\theta`$ is the initial phase and $`\phi(t)`$ is the excess phase at time $`t`$ which is described recursively as follows:

Let $`\phi_n`$ be the phase offset in the interval $`[nT,\ (n+1)T)`$, then $`\phi_n = \phi_{n-1} + \frac{\pi}{2}b_n`$. Simply put, this describes that the phase shifts by $`90^\circ`$ each time a 1 is transmitted and by $`-90^\circ`$ each time a -1 is transmitted.

 
### GMSK

Gaussian Minimum-Shift Keying (GMSK) is a special case of MSK where the signal is first shaped by a Gaussian filter which results in a decrease in the required bandwidth at the expense of increased intersymbol interference. 

A Gaussian filter has a impulse response given by $`h(t) = \frac{1}{\sigma T_s \sqrt{2\pi}}e^{-\frac{t^2}{2(\sigma T_s)^2}}`$, where $`\sigma = \frac{\sqrt{\ln(2)}}{2\pi BT_s}`$.
 
### Modulation

The modulator is based on the fact that any continuous phase signal can be decomposed into a finite number of PAM signals. As per [CCSDS 413.0-G-2](https://public.ccsds.org/Pubs/413x0g2s.pdf), we only use two PAM signals and thus the modulated signal is expressed in the form of

```math
x_I = A \sum_{n=-infty}^{n=\infty} b_{2n}C_0(t - 2nT_s) - b_{2n+1}b_{2n}b_{2n-1}C_1(t-2nT_s - T_s)

x_Q = A \sum_{n=-\infty}^{n=\infty} b_{2n+1}C_0(t - 2nT_s - T_s) - b_{2n}b_{2n-1}b_{2n-2}C_1(t-2nT_s)
```

where $`C_0, C_1`$ are the Gaussian pulses which are also described in [CCSDS 413.0-G-2](https://public.ccsds.org/Pubs/413x0g2s.pdf) for $`BT = 0.25`$ and $`BT = 0.5`$
