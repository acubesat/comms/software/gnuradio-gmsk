import math
import matplotlib.pyplot as plt
from scipy import signal
import random
import numpy as np


N = 40 # Length of bitstream
Nb = 8 # samples/symbol

BT = 0.25 # filter 3dB bandwidth * Ts

ntaps_c0 = 30
ntaps_c1 = 18

# Random input bitstream
bitstream_nrz = [2 * random.randrange(2) - 1 for i in range(N)]

# Precoding
alt_ones = np.array([2 * (i % 2 == 0) - 1 for i in range(N)])
bitstream = alt_ones * bitstream_nrz * np.roll(bitstream_nrz, 1)
bitstream[0] = 1

cn_odd = np.zeros(N*Nb)
cn_even = np.zeros(N*Nb)
cn_odd[Nb::2*Nb] = bitstream[1::2]
cn_even[::2*Nb] = bitstream[0::2]

bitstream_vn = bitstream * np.roll(bitstream, 1) * np.roll(bitstream, 2)
bitstream_vn[0] = bitstream_vn[1] = 1

vn_odd = np.zeros(N*Nb)
vn_even = np.zeros(N*Nb)
vn_odd[::2*Nb] = bitstream_vn[0::2]
vn_even[Nb::2*Nb] = bitstream_vn[1::2]

sigma = math.sqrt(math.log(2))/(2*math.pi*BT)
c0 = signal.gaussian(ntaps_c0, std = Nb*sigma)
c1 = signal.gaussian(ntaps_c1, std = Nb*sigma)
c1 = np.pad(c1, (0, ntaps_c0-ntaps_c1), 'constant')

out_i = 0.9 * signal.convolve(cn_even, c0)/sum(c0) - 0.1 * signal.convolve(vn_odd, c1)/sum(c1)
out_q = 0.9 * signal.convolve(cn_odd, c0)/sum(c0) - 0.1 * signal.convolve(vn_even, c1)/sum(c1)

r = (out_i**2 + out_q**2)**(1/2)
theta = np.arctan2(out_i, out_q)

# Plot signal
plt.plot(out_i, label='I')
plt.plot(out_q, label='Q')
plt.plot(((out_i)**2 + (out_q)**2)**(1/2), label='amplitude')
#plt.polar(theta, r)
plt.grid()
plt.legend()
plt.show()

# Plot filters
plt.plot(0.9 * c0, label='C0 filter')
plt.plot(0.1 * c1, label='C1 filter')
plt.grid()
plt.legend()
plt.show()
