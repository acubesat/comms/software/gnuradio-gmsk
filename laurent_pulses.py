import math
import matplotlib.pyplot as plt
from scipy import signal
import random
import numpy as np
import pickle
from gnuradio import filter
# CCSDS extrapolated Laurent Pulse (BT = 0.25)
with open('./Laurent_Pulses/C0_BT025.txt', 'rb') as fp:
    C0_BT025 = pickle.load(fp)

# CCSDS extrapolated Laurent Pulse (BT = 0.5)

with open('./Laurent_Pulses/C0_BT5.txt', 'rb') as fp:
    C0_BT5 = pickle.load(fp)

sps = 8
Ntaps = sps
gain = 1
BT = 0.5
gtaps2 = filter.firdes.gaussian(
    gain,     # gain
    sps,     # symbol_rate
    BT,   # bandwidth * symbol time
    Ntaps      # number of taps
)
sqwave = (1,) * sps  # rectangular window
gtaps2 = np.convolve(np.array(gtaps2), np.array(sqwave))
N_bits = 20
bitstream = [2 * random.randrange(2) - 1 for i in range(N_bits)]
Nb_1 = math.floor((len(gtaps2)/3)+1)  # This should be fixed, it is not the way to retrieve the symbol duration
odd = bitstream[1::2]
even = bitstream[0::2]
padded_stream_1 = np.zeros(N_bits * Nb_1)
padded_stream_2 = np.zeros(N_bits * Nb_1)
padded_stream_1[Nb_1::2 * Nb_1] = odd
padded_stream_2[0::2 * Nb_1] = even
taps = C0_BT5
output_1 = signal.convolve(padded_stream_1, gtaps2)
output_2 = signal.convolve(padded_stream_2, gtaps2)

r = (output_1 ** 2 + output_2 ** 2) ** (1 / 2)
res = np.sqrt(abs(1-np.square(r)))
theta = np.arctan2(output_2, output_1)
plt.polar(theta, r, label='BT =0.5')
plt.grid()
plt.legend()
plt.show()

# OUTPUT PLOT (BT = 0.5)

plt.plot(output_1, label='I (BT = 0.5)')
plt.plot(output_2, label='Q (BT = 0.5)')
plt.plot((output_1 ** 2 + output_2 ** 2) ** (1 / 2), label='amplitude (BT = 0.5)')
# plt.plot(res)
plt.grid()
plt.legend()
plt.show()


sps = 8
Ntaps = sps
gain = 1
BT = 0.25
gtaps1 = filter.firdes.gaussian(
    gain,     # gain
    sps,     # symbol_rate
    BT,   # bandwidth * symbol time
    Ntaps      # number of taps
)
sqwave = (1,) * sps  # rectangular window
gtaps1 = np.convolve(np.array(gtaps1), np.array(sqwave))
Nb_2 = math.floor((len(gtaps1)/3)+1)
padded_stream_1 = np.zeros(N_bits * Nb_2)
padded_stream_2 = np.zeros(N_bits * Nb_2)
padded_stream_1[Nb_2::2 * Nb_2] = odd
padded_stream_2[0::2 * Nb_2] = even
taps_2 = C0_BT025
output_1 = signal.convolve(padded_stream_1, gtaps1)
output_2 = signal.convolve(padded_stream_2, gtaps1)

r = (output_1 ** 2 + output_2 ** 2) ** (1 / 2)
theta = np.arctan2(output_2, output_1)
plt.polar(theta, r, label='BT =0.25')
plt.grid()
plt.legend()
plt.show()

# OUTPUT PLOT (BT = 0.25)
plt.plot(output_1, label='I (BT = 0.25)')
plt.plot(output_2, label='Q (BT = 0.25)')
plt.plot((output_1 ** 2 + output_2 ** 2) ** (1 / 2), label='amplitude (BT = 0.25)')
plt.grid()
plt.legend()
plt.show()

# The Laurent pulses
plt.plot(np.array(taps), label='From CCSDS: C0 Laurent Pulse (BT = 0.5)')
plt.plot(np.array(taps_2), label='From CCSDS: C0 Laurent Pulse (BT = 0.25)')
plt.grid()
plt.legend()
plt.show()
